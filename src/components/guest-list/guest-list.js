import React from 'react';

const GuestList = ({ guestList, isGuestArrived }) => {

  return(
    <ul className="guest-list">
      {guestList.map(guest => (
        <li key={guest.index}>
          <div className="guest-info">
            <p>Гость <span>{guest.name}</span> работает в компании <span>"{guest.company}"</span></p>
            <p>Его контакты: <br/> {guest.phone}</p>
            <p>{guest.address}</p>
          </div>
          <button className="btn-arrive" onClick={() => isGuestArrived(guest.index)} >Прибыл</button>
        </li>
      ))}
    </ul>
  )
}

export default GuestList;
