import React from 'react';
import './App.css';
import GuestList from "./guest-list/guest-list";
import guests from '../guests';

const modifiedData = guests.map( guest => {
  guest.arrived = false;
  return guest;
});

class App extends React.Component{

  state = {
    guestList: modifiedData
  }

  handleFilter = (e) => {

    let query = e.target.value.toLowerCase();

    let search = modifiedData.filter((item) => {
      let keys = Object.keys(item);
      delete keys[0];
      delete keys[5];
      let result = keys.some( key => {
        return item[key].toString().toLowerCase().indexOf(query) !== -1;
      });
      return result;
    });

    this.setState({
          guestList: search
        })
    console.log(search);

  }

  isGuestArrived = (index) => {

    let changedGuests = this.state.guestList.map( guest => {
      if( guest.index === index){
        guest.arrived = true;
      }
      return guest;
    }).filter( guest =>
      guest.arrived !== true
    );

    this.setState({
      guestList: changedGuests
    });
  }

  render(){
    const { guestList } = this.state;
    return (
      <div className="app">
        <div className="app-header">
          <h1 className="app-title">Список гостей <span>Список жертв</span></h1>
          <div className="img-search"></div>
        </div>

        <input className="input-search" type="text" placeholder="Введите имя гостя или его данные для поиска" onInput={this.handleFilter}/>
        { guestList ? <GuestList guestList={this.state.guestList} isGuestArrived={this.isGuestArrived}/> : <p>No matches with your search</p>}
      </div>
    );

  }

}

export default App;
